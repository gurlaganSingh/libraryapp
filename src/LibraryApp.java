import java.util.ArrayList;
import java.util.List;

public class LibraryApp {
	private boolean isLoggedIn = false;
	
	List<Book> books = new ArrayList<Book>();
	
	
	
	// class construtor
	public LibraryApp() {
		
	}
	public boolean adminLogin(String password) {
		//function assumes :
		// there is only 1 admin and pssword is adminadmin
		
		if(password.equals("adminadmin")) {
			this.isLoggedIn = true;
			
		
		}
		else {
			this.isLoggedIn = false;
			
		}
		return this.isLoggedIn;
	}
	public boolean adminLoggedIn() {
		return this.isLoggedIn;
	}
	
	public List<Book> getBooks() {
		//return something because of func empty
		return this.books;
	}
	public void addBook(Book book) throws OperationNotAllowedException {
		// TODO Auto-generated method stub
		
		if(this.isLoggedIn == true) {
			this.books.add(book);
		}else {
			throw new OperationNotAllowedException();
		}
		
	}

}
